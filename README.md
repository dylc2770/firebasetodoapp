# README #

This is a To Do app developed by me.

### How did I make it ###

I'm using Yeoman, Bower, Gulp, Git, jQuery, JavaScript, HTML & CSS to create this to-do app.
I also am planning to implement Firebase, to have a real-time database working with the app, also to add authentication and login functionality.

### How do I get set up? ###

* I will add instructions for setup once I'm further into the project.

### Who do I talk to? ###

* Dylan ( dylancarv@gmail.com )
