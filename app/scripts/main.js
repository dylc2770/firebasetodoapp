$(document).ready(function() {

  /*firebase*/
  //dbRef for the ToDos
  var dbRefToDos = firebase.database().ref().child('ToDos');

//when a child is added/loaded, pass through the data to the function:
  dbRefToDos.on('child_added', function(data){
    //get element with the id of toDos, this will be where the data will be appended to as children
    var toDos = document.getElementById('toDos');
    // create a div
    var item  = document.createElement('div');
    //with the class name of item
    item.className = 'item';
    //if the data grabbed from the database value has a true checked value:
    if(data.val().checked == true) {
    //then add the class item AND done
    item.className = 'item done';
    //also print the item in a different way sothat it is already checked off
    item.innerHTML ="<div class='text'>" + data.val().value + "</div><div class='check'><input type='checkbox' checked></div><div class='remove'>Remove</div><div class='clearfloat'></div>";

    } else {
    // else if the item is not checked, print it normally:
    item.innerHTML ="<div class='text'>" + data.val().value + "</div><div class='check'><input type='checkbox'></div><div class='hidden remove'>Remove</div><div class='clearfloat'></div>";
    }
    //append this element in the ToDos id element
    toDos.appendChild(item);
  });

  /*-------------------- checking an item --------------------*/
// this method of on click is used sothat it can be used on dynamically created elements (elements created by the js while grabbing the data from firebase)
  // When a checkbox is clicked
  $(document).on('click', '.check', function(){
    $(this).parent('.item').toggleClass('done');
    // toggle the class hidden from the remove class siblings
    $(this).siblings('.remove').toggleClass('hidden');
  });

  /*-------------------- removing an item --------------------*/
  /*
   *i'll have to replace this with firebase i think later instead of just hiding it
   *i will have to send a command to the firebase database to remove this specific entry
   */

  $(document).on('click', '.remove',function() {
    $(this).parent('.item').remove();
  });

  /*-------------------- FAB click popup --------------------*/
  $('#fab').click(function() {
    $('#popup').toggleClass('hidden');
  });
  /*-------------------- FAB click outside of popup (to go back) --------------------*/
  //detect if the mouse is inside or outside the center area
  var mouse_is_inside = false;
  $('.center').hover(function() {
    //mouse is inside
    mouse_is_inside = true;
  }, function() {
    //mouse is outside
    mouse_is_inside = false;
  });

  //when #popup is clicked
  $('#popup').click(function() {
    //if mouse is outside of the .center area
    if (mouse_is_inside == false) {
      //hide the #popup again
      $('#popup').toggleClass('hidden');
    };
  });

  // end ready function
});
